package com.example.notificaciones.Activitys.Visual;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.example.notificaciones.Activitys.Menu;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.CrVisual;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Menu_Visual extends AppCompatActivity {


    Button Vus, V;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__visual);

        int us1,al1;
        String us,al;
        Bundle bundle =  this.getIntent().getExtras();
        if(bundle != null)
        {
            us = bundle.getString("alertaId");
            al = bundle.getString("usId");
            //Toast.makeText(Menu_Visual.this,""+al+" "+us,Toast.LENGTH_LONG).show();

            us1 = Integer.parseInt(al);
            al1 = Integer.parseInt(us);

            ServicioPeticion servicioPeticion = Api.getApi(Menu_Visual.this).create(ServicioPeticion.class);
            Call<CrVisual>crVisualCall = servicioPeticion.crearVisual(us1,al1);
            crVisualCall.enqueue(new Callback<CrVisual>() {
                @Override
                public void onResponse(Call<CrVisual> call, Response<CrVisual> response) {
                    CrVisual peticion = response.body();

                    if(response.body() == null)
                    {
                        Toast.makeText(Menu_Visual.this,"Ha ocurrido un error",Toast.LENGTH_LONG).show();
                    }
                    if(peticion.getEstado() == "true")
                    {
                        Toast.makeText(Menu_Visual.this,"Visualizacion Creada exitosamente",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(Menu_Visual.this,peticion.getDetalle(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<CrVisual> call, Throwable t) {
                    Toast.makeText(Menu_Visual.this,"Error",Toast.LENGTH_LONG).show();
                }
            });
        }

        V = (Button) findViewById(R.id.btnTodasVis);
        Vus = (Button) findViewById(R.id.btnVisual_us);

        V.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_Visual.this, Visualizaciones.class));
            }
        });

        Vus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_Visual.this, usuario_visual.class));
            }
        });
    }
}
