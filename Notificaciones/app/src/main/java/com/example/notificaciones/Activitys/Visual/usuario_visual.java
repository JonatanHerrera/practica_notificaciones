package com.example.notificaciones.Activitys.Visual;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Adaptadores.Adaptador_visual_usuario;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.Visualizaciones_usuario;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class usuario_visual extends AppCompatActivity {

    private int id;
    RecyclerView usuario_visual;
    Adaptador_visual_usuario adaptador_visual_usuario;
    Button regresar;
    ServicioPeticion servicioPeticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_visual);

        SharedPreferences preferences  = getSharedPreferences("NumeroId", Context.MODE_PRIVATE);
        String i = preferences.getString("Id","");
        id = Integer.parseInt(i);

        regresar = findViewById(R.id.Regresar);

        usuario_visual = findViewById(R.id.recyclerView);
        usuario_visual.setHasFixedSize(true);
        usuario_visual.setLayoutManager(new LinearLayoutManager(usuario_visual.this));

        adaptador_visual_usuario = new Adaptador_visual_usuario(this);

        servicioPeticion = Api.getApi(usuario_visual.this).create(ServicioPeticion.class);
        servicioPeticion.visual_usuario(id).enqueue(new Callback<Visualizaciones_usuario>() {
            @Override
            public void onResponse(Call<Visualizaciones_usuario> call, Response<Visualizaciones_usuario> response) {
                if(response.body() == null)
                {
                    Toast.makeText(usuario_visual.this,"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
                }

                if(response.body().getEstado() == "true")
                {
                    List<Visualizaciones_usuario.Dato> datos = response.body().getVisualizaciones();
                    adaptador_visual_usuario.Guardar(datos);
                    usuario_visual.setAdapter(adaptador_visual_usuario);
                }
            }

            @Override
            public void onFailure(Call<Visualizaciones_usuario> call, Throwable t) {
                Toast.makeText(usuario_visual.this,"Error",Toast.LENGTH_SHORT).show();
            }
        });

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(usuario_visual.this,Menu_Visual.class));
            }
        });

    }
}
