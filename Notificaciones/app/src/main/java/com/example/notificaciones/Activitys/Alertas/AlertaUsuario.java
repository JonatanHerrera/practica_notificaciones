package com.example.notificaciones.Activitys.Alertas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Adaptadores.Adaptador_alerta_usuario;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.Alerta_Usuario;
import com.example.notificaciones.ViewModel.Todas_alertas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertaUsuario extends AppCompatActivity {

    private int id;
    RecyclerView Alertas;
    Adaptador_alerta_usuario adaptador_alerta_usuario;//Adaptador_alerta_u
    Button regresar;
    ServicioPeticion servicioPeticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerta_usuario);

        SharedPreferences preferences  = getSharedPreferences("NumeroId", Context.MODE_PRIVATE);
        String i = preferences.getString("Id","");
        id = Integer.parseInt(i);

        regresar = findViewById(R.id.regresar);

        Alertas = findViewById(R.id.recyclerView);
        Alertas.setHasFixedSize(true);
        Alertas.setLayoutManager(new LinearLayoutManager(AlertaUsuario.this));

        adaptador_alerta_usuario = new Adaptador_alerta_usuario(this);

        servicioPeticion = Api.getApi(AlertaUsuario.this).create(ServicioPeticion.class);
        servicioPeticion.alertaUsuario(id).enqueue(new Callback<Alerta_Usuario>() {
            @Override
            public void onResponse(Call<Alerta_Usuario> call, Response<Alerta_Usuario> response) {
                Alerta_Usuario peticion = response.body();
                if(response.body() == null)
                {
                    Toast.makeText(AlertaUsuario.this,"Ha ocurrido un error",Toast.LENGTH_LONG).show();
                }
                if(peticion.getEstado() == "true")
                {
                    List<Alerta_Usuario.Datos> datos = response.body().getAlertas();
                    adaptador_alerta_usuario.Guardar(datos);
                    Alertas.setAdapter(adaptador_alerta_usuario);
                }
            }

            @Override
            public void onFailure(Call<Alerta_Usuario> call, Throwable t) {
                Toast.makeText(AlertaUsuario.this,"Error",Toast.LENGTH_LONG).show();
            }
        });

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AlertaUsuario.this, Menu_Alertas.class));
            }
        });

    }
}
