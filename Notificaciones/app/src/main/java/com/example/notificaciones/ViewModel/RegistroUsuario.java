package com.example.notificaciones.ViewModel;

public class RegistroUsuario {
    //Declaracion de las variables de respuesta de usuario
    public String estado;
    public String correo;
    public String password;
    public String detalle;


    //Metodo constructor
    public RegistroUsuario(){}

    public RegistroUsuario(String correo, String password) {
        this.correo = correo;
        this.password = password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
