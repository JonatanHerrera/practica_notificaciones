package com.example.notificaciones.Servicios;

import com.example.notificaciones.ViewModel.Alerta_Usuario;
import com.example.notificaciones.ViewModel.CrAlerta;
import com.example.notificaciones.ViewModel.CrVisual;
import com.example.notificaciones.ViewModel.Login;
import com.example.notificaciones.ViewModel.RegistroUsuario;
import com.example.notificaciones.ViewModel.Todas_alertas;
import com.example.notificaciones.ViewModel.Todas_visualizaciones;
import com.example.notificaciones.ViewModel.Visualizaciones_usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuario> registro(@Field("username")String usuario, @Field("password")String contra);

    @FormUrlEncoded
    @POST("api/login")
    Call<Login> Entrar(@Field("username")String usuario, @Field("password")String contra);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<CrAlerta> crearAlerta(@Field("usuarioId")int id);

    @FormUrlEncoded
    @POST("api/alertas")
    Call<Todas_alertas> todas(@Field("username")String user);

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<Alerta_Usuario> alertaUsuario(@Field("usuarioId")int usuarioId);

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<CrVisual> crearVisual(@Field("usuarioId")int idUsuario, @Field("alertaId")int idAlerta);

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Visualizaciones_usuario> visual_usuario(@Field("usuarioId")int usuarioId);

    @FormUrlEncoded
    @POST("api/visualizaciones")
    Call<Todas_visualizaciones> Todas_visual(@Field("username") String user);

}
