package com.example.notificaciones.Activitys;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.notificaciones.Activitys.Visual.Menu_Visual;
import com.example.notificaciones.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import static android.app.PendingIntent.getActivity;

public class Fcm extends FirebaseMessagingService {

    String UsId, AlId;
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        GuardarToken(s);
    }

    public void GuardarToken(String s)
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("usuarioId").setValue(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if(remoteMessage.getData().size() > 0)
        {
            String usid,alid;

            usid = remoteMessage.getData().get("usuarioId");
            alid = remoteMessage.getData().get("alertaId");

            UsId = usid;
            AlId = alid;

            String titulo = remoteMessage.getData().get("alertaId");
            String detalle = remoteMessage.getData().get("usuarioId");
            Version8Y9(titulo,detalle);
        }

    }

    private void Version8Y9(String titulo, String detalle) {
        String id = "mensaje";
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,id);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel nc = new NotificationChannel(id, "nuevo",NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert nm != null;
            nm.createNotificationChannel(nc);
        }

        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(clicknoti())
                .setContentInfo("nuevo");

        Random random = new Random();
        int idNot = random.nextInt(8000);
        assert nm != null;
        nm.notify(idNot,builder.build());
    }

    public PendingIntent clicknoti()
    {
        Intent intent = new Intent(this, Menu_Visual.class);
        intent.putExtra("usId",UsId);
        intent.putExtra("alertaId",AlId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this,0,intent,0);
    }
}
