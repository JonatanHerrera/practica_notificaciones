package com.example.notificaciones.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notificaciones.R;
import com.example.notificaciones.ViewModel.Todas_visualizaciones;

import java.util.List;

public class Adaptador_todas_visual extends RecyclerView.Adapter<Adaptador_todas_visual.MVH> {

    private List<Todas_visualizaciones.Datos> todas_visual_list;// lista_todas_visualizaciones
    private Context context;
    private MVH mvh;

    public Adaptador_todas_visual(Context context){}

    public void setData(Context context){this.context = context;}

    @NonNull
    @Override
    public Adaptador_todas_visual.MVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interfaz_visual,parent,false);
        return new MVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptador_todas_visual.MVH holder, int position) {
        String id1,idUs1,idAl1;

        id1 = ""+todas_visual_list.get(position).getId();
        idUs1 = "Id usuario:"+todas_visual_list.get(position).getUsuarioId();
        idAl1 = "Id alerta:"+todas_visual_list.get(position).getAlertaId();

        holder.id.setText(id1);
        holder.idUs.setText(idUs1);
        holder.idAl.setText(idAl1);

    }

    @Override
    public int getItemCount() {
        return todas_visual_list.size();
    }

    public void Guardar(List<Todas_visualizaciones.Datos> todas_visual_list)
    {
        this.todas_visual_list = todas_visual_list;
    }

    public class MVH extends RecyclerView.ViewHolder {
        TextView id,idUs,idAl;
        public MVH(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            idUs = itemView.findViewById(R.id.idUsuario);
            idAl = itemView.findViewById(R.id.idAlerta);
        }
    }
}
