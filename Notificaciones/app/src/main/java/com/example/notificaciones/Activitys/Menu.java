package com.example.notificaciones.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Activitys.Alertas.Menu_Alertas;
import com.example.notificaciones.Activitys.Visual.Menu_Visual;
import com.example.notificaciones.Activitys.Visual.Menu_visual2;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.CrVisual;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    private Button a,v,n;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        a = (Button) findViewById(R.id.btnAlertas);
        v = (Button) findViewById(R.id.btnVisual);
        n = (Button) findViewById(R.id.btnNotificaciones);

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Menu_Alertas.class));
            }
        });

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Menu_Visual.class));
            }
        });

        n.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this,Notificaciones.class));
            }
        });
    }
}
