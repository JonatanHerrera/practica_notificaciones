package com.example.notificaciones.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notificaciones.Activitys.Visual.Visualizaciones;
import com.example.notificaciones.R;
import com.example.notificaciones.ViewModel.Visualizaciones_usuario;

import java.util.List;

public class Adaptador_visual_usuario extends RecyclerView.Adapter<Adaptador_visual_usuario.MVH> {

    private List<Visualizaciones_usuario.Dato> usuario_visual_list;
    private Context context;
    private MVH mvh;

    public Adaptador_visual_usuario(Context context){}

    public void setData(Context context){this.context = context;}
    @NonNull
    @Override
    public MVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interfaz_visual,parent,false);
        return new MVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MVH holder, int position) {
        String id1,idUs1,idAl1;

        id1 = ""+usuario_visual_list.get(position).getId();
        idUs1 = "id Usuario:"+usuario_visual_list.get(position).getUsuarioId();
        idAl1 = "id Alerta:"+usuario_visual_list.get(position).getAlertaId();

        holder.id.setText(id1);
        holder.idUs.setText(idUs1);
        holder.idAl.setText(idAl1);

    }

    @Override
    public int getItemCount() {
        return usuario_visual_list.size();
    }

    public void Guardar (List<Visualizaciones_usuario.Dato> usuario_visual_list)
    {
        this.usuario_visual_list = usuario_visual_list;
        notifyDataSetChanged();
    }

    public class MVH extends RecyclerView.ViewHolder {
        TextView id,idUs,idAl;
        public MVH(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            idUs = itemView.findViewById(R.id.idUsuario);
            idAl = itemView.findViewById(R.id.idAlerta);
        }
    }
}
