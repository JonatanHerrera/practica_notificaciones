package com.example.notificaciones.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button login,CrearUsuario;
    private EditText us, co;

    private String ApiId, ApiToken;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token = preferences.getString("TOKEN","");

        if(token!= "")
        {
            //Toast.makeText(MainActivity.this,"Bienvenido de nuevo",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, Menu.class);
            startActivity(intent);
        }

        login = (Button)findViewById(R.id.btnLogin);
        CrearUsuario = (Button) findViewById(R.id.btnregistro);

        us = (EditText) findViewById(R.id.txtUs);
        co = (EditText) findViewById(R.id.txtContra);



        CrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Registro.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(us.getText().toString().isEmpty() || co.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this, " Campos vacios",Toast.LENGTH_LONG).show();
                }
                else
                {
                    ServicioPeticion servicioPeticion = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                    Call<Login> loginCall = servicioPeticion.Entrar(us.getText().toString(), co.getText().toString());
                    loginCall.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            Login peticion = response.body();

                            if(response.body() == null)
                            {
                                Toast.makeText(MainActivity.this, "Ocurrio un error con el servidor, intente mas tarde", Toast.LENGTH_LONG).show();
                                return;
                            }

                            if(peticion.estado=="true")
                            {
                                ApiToken = peticion.token;
                                ApiId = ""+peticion.id;
                                GuardarToken();
                                GuardarId();
                                Toast.makeText(MainActivity.this,"Bienvenido", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MainActivity.this, Menu.class);
                                startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, "Datos incorrectos",Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void GuardarToken()
    {
        SharedPreferences preferencesToken = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = ApiToken;
        SharedPreferences.Editor editor = preferencesToken.edit();
        editor.putString("TOKEN",token);
        editor.commit();
    }

    private void GuardarId()
    {
        SharedPreferences preferencesToken = getSharedPreferences("NumeroId", Context.MODE_PRIVATE);
        String id = ApiId;
        SharedPreferences.Editor editor = preferencesToken.edit();
        editor.putString("Id",id);
        editor.commit();
    }
}
