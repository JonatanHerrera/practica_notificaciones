package com.example.notificaciones.Activitys.Visual;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.CrVisual;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu_visual2 extends AppCompatActivity {

    Button Vus, V;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_visual2);

        /*
        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            Toast.makeText(Menu_visual2.this,"Se reciben datos",Toast.LENGTH_SHORT).show();
            idus = Integer.parseInt(bundle.getString("usId"));
            idal = Integer.parseInt(bundle.getString("alId"));

            Toast.makeText(Menu_visual2.this,""+idus+" "+idal,Toast.LENGTH_SHORT).show();


            ServicioPeticion servicioPeticion = Api.getApi(Menu_visual2.this).create(ServicioPeticion.class);
            Call<CrVisual> crVisualCall = servicioPeticion.crearVisual(idus,idal);
            crVisualCall.enqueue(new Callback<CrVisual>() {
                @Override
                public void onResponse(Call<CrVisual> call, Response<CrVisual> response) {
                    CrVisual peticion = response.body();
                    if(response.body() == null)
                    {
                        Toast.makeText(Menu_visual2.this,"Ha ocurrido un erro, intentelo mas tarde",Toast.LENGTH_SHORT).show();
                    }
                    if(peticion.getEstado() == "true")
                    {
                        Toast.makeText(Menu_visual2.this,"Visualizacion creada con exito",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(Menu_visual2.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<CrVisual> call, Throwable t) {
                    Toast.makeText(Menu_visual2.this,"Error",Toast.LENGTH_SHORT).show();
                }
            });


        }

         */

        V = (Button) findViewById(R.id.btnTodasVis);
        Vus = (Button) findViewById(R.id.btnVisual_us);

        V.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_visual2.this, Visualizaciones.class));
            }
        });

        Vus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_visual2.this, usuario_visual.class));
            }
        });
    }
}
