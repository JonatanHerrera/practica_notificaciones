package com.example.notificaciones.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notificaciones.R;
import com.example.notificaciones.ViewModel.Todas_alertas;

import java.util.List;

public class Adaptador_Todas_Alertas extends RecyclerView.Adapter<Adaptador_Todas_Alertas.MVH> {

    private List<Todas_alertas.Datos> TodasList; //ListTodas
    private Context context;
    private MVH mvh;

    public Adaptador_Todas_Alertas(Context context){}

    public void setData(Context context) {this.context = context;}
    @NonNull
    @Override
    public MVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interfaz_alertas,parent,false);
        return new MVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MVH holder, int position) {
        String id, idUs;
        id = ""+TodasList.get(position).getId();
        idUs = "Id usuario: "+TodasList.get(position).getUsuarioId();
        holder.idAlerta.setText(id);
        holder.idUsuario.setText(idUs);
        holder.creacion.setText(TodasList.get(position).getCreated_at());
    }

    @Override
    public int getItemCount() {
        return TodasList.size();
    }

    public void Guardar(List<Todas_alertas.Datos> todasList)
    {
        this.TodasList = todasList;
        notifyDataSetChanged();
    }

    public class MVH extends RecyclerView.ViewHolder {
        TextView idAlerta,idUsuario,creacion;
        public MVH(@NonNull View itemView) {
            super(itemView);
            idAlerta = itemView.findViewById(R.id.idAlerta);
            idUsuario = itemView.findViewById(R.id.idUsuario);
            creacion = itemView.findViewById(R.id.Creacion);
        }
    }
}
