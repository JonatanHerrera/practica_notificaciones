package com.example.notificaciones.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.notificaciones.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;
import java.util.HashMap;
import java.util.Map;

public class Notificaciones extends AppCompatActivity {

    private Button general, especifica;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);

        general = (Button) findViewById(R.id.btnGeneral);
        especifica = (Button) findViewById(R.id.btnEspecifica);


        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                General();
            }
        });

        especifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Especifico();
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Notificaciones.this,"Se agrego al grupo",Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void Especifico()
    {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try
        {
            String token = "c9jUDbpCTrKXbD1VdOddpL:APA91bEXULMO86MVXpCjC6vSRcLnUBGX-RZ8mvlEzkI6n0RfPV7eW0rcRnc64ysiJgOgk0hSWtF08U1Qs_nzeZG3PlVrWFr5HgKDLU5tHdqieAXWnK8qmRdD6VkUVBB8XAGS5Fxqm8xd";
            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","Mostrando un titulo");
            notificacion.put("detalle","Mostrando el contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null)
            {
                @Override
                public Map<String, String> getHeaders()
                {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAswfpgIE:APA91bEr8XwFIGngGQfEdc9QRByfD5NhvuxnxHQVpyjGxQg_58l_xhyiXq1kkG5RpWKNSGbPVvR-QAUACQKGgjCIyt-GkhJOQPBco7NonJ6UHF8QVx9QyVrZ5pt1wXg93SkDPUlrJzAt");
                    return  header;
                }
            };
            myrequest.add(request);
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void General()
    {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try
        {
            //String token = "c9jUDbpCTrKXbD1VdOddpL:APA91bEXULMO86MVXpCjC6vSRcLnUBGX-RZ8mvlEzkI6n0RfPV7eW0rcRnc64ysiJgOgk0hSWtF08U1Qs_nzeZG3PlVrWFr5HgKDLU5tHdqieAXWnK8qmRdD6VkUVBB8XAGS5Fxqm8xd";
            json.put("to","/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo","Mostrando un titulo");
            notificacion.put("detalle","Mostrando el contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null,null)
            {
                @Override
                public Map<String, String> getHeaders()
                {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAswfpgIE:APA91bEr8XwFIGngGQfEdc9QRByfD5NhvuxnxHQVpyjGxQg_58l_xhyiXq1kkG5RpWKNSGbPVvR-QAUACQKGgjCIyt-GkhJOQPBco7NonJ6UHF8QVx9QyVrZ5pt1wXg93SkDPUlrJzAt");
                    return  header;
                }
            };
            myrequest.add(request);
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }


}
