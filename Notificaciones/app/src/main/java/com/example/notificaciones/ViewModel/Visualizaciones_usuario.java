package com.example.notificaciones.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class Visualizaciones_usuario {

    public String estado;
    public List<Dato> visualizaciones = new ArrayList<>();

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Dato> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Dato> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public class Dato
    {
        public int id;
        public int usuarioId;
        public int alertaId;
        public String created_at;
        public String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUsuarioId() {
            return usuarioId;
        }

        public void setUsuarioId(int usuarioId) {
            this.usuarioId = usuarioId;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getAlertaId() {
            return alertaId;
        }

        public void setAlertaId(int alertaId) {
            this.alertaId = alertaId;
        }
    }
}
