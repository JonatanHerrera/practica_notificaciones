package com.example.notificaciones.Activitys.Alertas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Adaptadores.Adaptador_Todas_Alertas;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.Todas_alertas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodasAlertas extends AppCompatActivity {

    RecyclerView Alertas;
    Adaptador_Todas_Alertas adaptador_todas_alertas;
    Button regresar;
    ServicioPeticion servicioPeticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todas_alertas);

        regresar = findViewById(R.id.regresar);

        Alertas = findViewById(R.id.recyclerView);

        Alertas.setHasFixedSize(true);
        Alertas.setLayoutManager(new LinearLayoutManager(TodasAlertas.this));

        adaptador_todas_alertas = new Adaptador_Todas_Alertas(this);

        servicioPeticion = Api.getApi(TodasAlertas.this).create(ServicioPeticion.class);
        servicioPeticion.todas("s").enqueue(new Callback<Todas_alertas>() {
            @Override
            public void onResponse(Call<Todas_alertas> call, Response<Todas_alertas> response) {
                Todas_alertas peticion = response.body();
                if(response.body() == null)
                {
                    Toast.makeText(TodasAlertas.this,"Ha ocurrido un error",Toast.LENGTH_LONG).show();
                }
                    List<Todas_alertas.Datos> datos = response.body().getAlertas();
                    adaptador_todas_alertas.Guardar(datos);
                    Alertas.setAdapter(adaptador_todas_alertas);
            }

            @Override
            public void onFailure(Call<Todas_alertas> call, Throwable t) {
                Toast.makeText(TodasAlertas.this,"Error",Toast.LENGTH_LONG).show();
            }
        });



        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TodasAlertas.this, Menu_Alertas.class));
            }
        });

    }
}
