package com.example.notificaciones.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notificaciones.Activitys.Alertas.AlertaUsuario;
import com.example.notificaciones.R;
import com.example.notificaciones.ViewModel.Alerta_Usuario;

import java.util.List;

public class Adaptador_alerta_usuario extends RecyclerView.Adapter<Adaptador_alerta_usuario.MVH> {

    private List<Alerta_Usuario.Datos> alerta_usuario_list;
    private Context context;
    private MVH mvh;

    public Adaptador_alerta_usuario(Context context){}

    public void setData(Context context){this.context = context;}
    @NonNull
    @Override
    public MVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interfaz_alertas,parent,false);
        return new MVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MVH holder, int position) {
        String id, idUs;
        id = ""+alerta_usuario_list.get(position).getId();
        idUs = "id Usuario:"+alerta_usuario_list.get(position).getUsuarioId();
        holder.idAlerta.setText(id);
        holder.idUsuario.setText(idUs);
        holder.creacion.setText(alerta_usuario_list.get(position).getCreated_at());
    }

    @Override
    public int getItemCount() {
        return alerta_usuario_list.size();
    }

    public void Guardar(List<Alerta_Usuario.Datos> alerta_usuario_list )
    {
        this.alerta_usuario_list = alerta_usuario_list;
        notifyDataSetChanged();
    }

    public class MVH extends RecyclerView.ViewHolder {
        TextView idAlerta,idUsuario,creacion;
        public MVH(@NonNull View itemView) {
            super(itemView);
            idAlerta = itemView.findViewById(R.id.idAlerta);
            idUsuario = itemView.findViewById(R.id.idUsuario);
            creacion = itemView.findViewById(R.id.Creacion);
        }
    }
}
