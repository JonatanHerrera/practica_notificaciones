package com.example.notificaciones.Activitys.Alertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.CrAlerta;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu_Alertas extends AppCompatActivity {

    private int id;
    private Button crear,VerUs, VerTodo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__alertas);

        SharedPreferences preferences  = getSharedPreferences("NumeroId", Context.MODE_PRIVATE);
        String i = preferences.getString("Id","");//a
        id = Integer.parseInt(i);

        crear = (Button) findViewById(R.id.btnCrearAlerta);
        VerUs = (Button) findViewById(R.id.btnAlertaUsuario);
        VerTodo = (Button) findViewById(R.id.btnTodasAlertas);

        VerTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_Alertas.this, TodasAlertas.class));
            }
        });

        VerUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu_Alertas.this, AlertaUsuario.class));
            }
        });

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion servicioPeticion = Api.getApi(Menu_Alertas.this).create(ServicioPeticion.class);
                Call<CrAlerta> crAlertaCall = servicioPeticion.crearAlerta(id);
                crAlertaCall.enqueue(new Callback<CrAlerta>() {
                    @Override
                    public void onResponse(Call<CrAlerta> call, Response<CrAlerta> response) {
                        CrAlerta peticion = response.body();
                        if(response.body() == null)
                        {
                            Toast.makeText(Menu_Alertas.this, "Ocurrio un error con el servidor, intentelo mas tarde", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if(peticion.estado == "true")
                        {
                            Toast.makeText(Menu_Alertas.this,"Alerta creada con exito",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(Menu_Alertas.this,"No se pudo crear la alerta",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CrAlerta> call, Throwable t) {
                        Toast.makeText(Menu_Alertas.this,"Error",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
