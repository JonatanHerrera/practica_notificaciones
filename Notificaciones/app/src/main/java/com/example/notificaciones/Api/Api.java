package com.example.notificaciones.Api;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    //Se declara la URL base
    private static final String BASEURL = "https://notificacionupt.andocodeando.net/";
    //Declaraacion de una variable de tipo String vacia llamada token
    public static String token = "";

    public Api(){

    }

    //Se crea una instancia de la clase Retrofit
    private static Retrofit retrofit = null;

    public static Retrofit getApi(Context context) {
        if (retrofit == null) {

            //Se envian los datos de el archivo llamado credenciales, en concreto la variable TOKEN
            SharedPreferences preferencias = context.getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            token = preferencias.getString("TOKEN", "");

            //Construccion de la peticion al servicio
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            //Se solicita una peticion
            retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        } else {
            return retrofit;
        }
    }
}
