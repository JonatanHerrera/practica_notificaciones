package com.example.notificaciones.Activitys.Visual;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.notificaciones.Adaptadores.Adaptador_todas_visual;
import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.Todas_visualizaciones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizaciones extends AppCompatActivity {

    RecyclerView Visual;
    Adaptador_todas_visual adaptador_todas_visual;
    Button regreso;
    ServicioPeticion servicioPeticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);

        regreso = findViewById(R.id.Regresar);

        Visual = findViewById(R.id.recyclerView);
        Visual.setHasFixedSize(false);
        Visual.setLayoutManager(new LinearLayoutManager(Visualizaciones.this));

        adaptador_todas_visual = new Adaptador_todas_visual(this);

        servicioPeticion = Api.getApi(Visualizaciones.this).create(ServicioPeticion.class);
        servicioPeticion.Todas_visual("a").enqueue(new Callback<Todas_visualizaciones>() {
            @Override
            public void onResponse(Call<Todas_visualizaciones> call, Response<Todas_visualizaciones> response) {
                if(response.body() == null)
                {
                    Toast.makeText(Visualizaciones.this,"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
                }

                List<Todas_visualizaciones.Datos> datos = response.body().getVisualizaciones();
                adaptador_todas_visual.Guardar(datos);
                Visual.setAdapter(adaptador_todas_visual);
            }

            @Override
            public void onFailure(Call<Todas_visualizaciones> call, Throwable t) {
                Toast.makeText(Visualizaciones.this,"Error",Toast.LENGTH_SHORT).show();
            }
        });

        regreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Visualizaciones.this,Menu_Visual.class));
            }
        });
    }
}
