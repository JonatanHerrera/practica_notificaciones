package com.example.notificaciones.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class Todas_alertas {

    public List<Datos> alertas = new ArrayList<>();

    public List<Datos> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Datos> alertas) {
        this.alertas = alertas;
    }

    public class Datos
    {
        public int id;
        public int usuarioId;
        public String created_at;
        public String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUsuarioId() {
            return usuarioId;
        }

        public void setUsuarioId(int usuarioId) {
            this.usuarioId = usuarioId;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
