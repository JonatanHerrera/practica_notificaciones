package com.example.notificaciones.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificaciones.Api.Api;
import com.example.notificaciones.R;
import com.example.notificaciones.Servicios.ServicioPeticion;
import com.example.notificaciones.ViewModel.RegistroUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    private EditText u,c1,c2;
    private Button r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        r = (Button) findViewById(R.id.btnRegis);
        u = (EditText) findViewById(R.id.txtUsuario);
        c1 = (EditText)findViewById(R.id.txtContra1);
        c2 = (EditText)findViewById(R.id.txtContra2);

        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(u.getText().toString().isEmpty() || c1.getText().toString().isEmpty() || c2.getText().toString().isEmpty())
                {
                    Toast.makeText(Registro.this,"Campos vacios, favor de llenar todos los datos",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if(c1.getText().toString().equals(c2.getText().toString()))
                    {
                        ServicioPeticion servicioPeticion = Api.getApi(Registro.this).create(ServicioPeticion.class);
                        Call<RegistroUsuario> registroCall = servicioPeticion.registro(u.getText().toString(),c1.getText().toString());
                        registroCall.enqueue(new Callback<RegistroUsuario>() {
                            @Override
                            public void onResponse(Call<RegistroUsuario> call, Response<RegistroUsuario> response) {
                                RegistroUsuario peticion = response.body();

                                if(response.body() == null)
                                {
                                    Toast.makeText(Registro.this,"Ha ocurrido un error, intentelo mas tarde",Toast.LENGTH_LONG).show();
                                }
                                if(peticion.estado=="true")
                                {
                                    startActivity(new Intent(Registro.this, MainActivity.class));
                                    Toast.makeText(Registro.this,"Registro exitoso",Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<RegistroUsuario> call, Throwable t) {
                                Toast.makeText(Registro.this,"Error",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(Registro.this,"Las contraseñas no coinciden",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }
}
